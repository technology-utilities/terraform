ARG TERRAFORM_VERSION
ARG TERRAFORM_MAJOR_VERSION=
FROM registry.gitlab.com/gitlab-org/terraform-images/releases/${TERRAFORM_MAJOR_VERSION}:latest as gitlab_bulder

FROM alpine
ARG TERRAFORM_VERSION
ARG TERRAFORM_MAJOR_VERSION
WORKDIR /app

RUN apk add --no-cache --update ca-certificates \
      && apk add --no-cache --update -t deps curl jq aws-cli bash git \
      && curl -sL https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -o /tmp/terraform.zip \
      && unzip /tmp/terraform.zip -d /usr/local/bin/ \
      && chmod +x /usr/local/bin/terraform \
      && apk del --purge deps \
      && rm /tmp/terraform.zip

COPY --from=gitlab_bulder /usr/bin/gitlab-terraform /usr/bin/gitlab-terraform
